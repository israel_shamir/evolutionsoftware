﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using EvolutionLb1;
namespace Tests
{

    [TestFixture]
    public class Tests
    {
        [Test()]
        public void Test1()
        {
            Goods cola = new Goods("Cola", Goods.REGULAR);
            Goods pepsi = new Goods("Pepsi", Goods.SALE);
            Goods fanta = new Goods("Fanta", Goods.SPECIAL_OFFER);

            Item i1 = new Item(cola, 6, 65);
            Item i2 = new Item(pepsi, 3, 50);
            Item i3 = new Item(fanta, 3, 44);

            Customer x = new Customer("test", 10);

            Bill b1 = new Bill(x);
            b1.addGoods(i1);
            b1.addGoods(i2);
            b1.addGoods(i3);

            string bill = b1.statement();

            Assert.AreEqual("Счет для test\n\tНазвание\tЦена\tКол-воСтоимость\tСкидка\tСумма\tБонус\n\tCola\t\t65\t6\t390\t11,7\t380\t19\n\tPepsi\t\t50\t3\t150\t0\t150\t1\n\tFanta\t\t44\t3\t132\t0\t132\t0\nСумма счета составляет 662\nВы заработали 20 бонусных балов", bill);

        }

        [Test()]
        public void Test2()
        {
            Goods cola = new Goods("Cola", Goods.REGULAR);
            Goods pepsi = new Goods("Pepsi", Goods.SALE);
            Goods fanta = new Goods("Fanta", Goods.SPECIAL_OFFER);

            Item i1 = new Item(cola, 6, 65);
            Item i2 = new Item(pepsi, 3, 50);
            Item i3 = new Item(fanta, 12, 44);

            Customer x = new Customer("test", 10);

            Bill b1 = new Bill(x);
            b1.addGoods(i1);
            b1.addGoods(i2);
            b1.addGoods(i3);

            string bill = b1.statement();

            Assert.AreEqual("Счет для test\n\tНазвание\tЦена\tКол-воСтоимость\tСкидка\tСумма\tБонус\n\tCola\t\t65\t6\t390\t11,7\t380\t19\n\tPepsi\t\t50\t3\t150\t0\t150\t1\n\tFanta\t\t44\t12\t528\t2,64\t528\t0\nСумма счета составляет 1058\nВы заработали 20 бонусных балов", bill);
        }

        [Test()]
        public void Test3()
        {
            Goods cola = new Goods("Cola", Goods.REGULAR);
            Item i1 = new Item(cola, 6, 65);
            Customer x = new Customer("test", 10);

            Bill b1 = new Bill(x);
            b1.addGoods(i1);


            string bill = b1.statement();

            Assert.AreEqual("Счет для test\n\tНазвание\tЦена\tКол-воСтоимость\tСкидка\tСумма\tБонус\n\tCola\t\t65\t6\t390\t11,7\t380\t19\nСумма счета составляет 380\nВы заработали 19 бонусных балов", bill);
        }

        [Test()]
        public void Test4()
        {
            Goods cola = new Goods("Cola", Goods.REGULAR);
            Item i1 = new Item(cola, 1, 65); //количество, стоимость
            Customer x = new Customer("test", 10);

            Bill b1 = new Bill(x);
            b1.addGoods(i1);


            string bill = b1.statement();
            Assert.AreEqual("Счет для test\n\tНазвание\tЦена\tКол-воСтоимость\tСкидка\tСумма\tБонус\n\tCola\t\t65\t1\t65\t0\t65\t3\nСумма счета составляет 65\nВы заработали 3 бонусных балов", bill);

        }
        [Test()]
        public void Test5()
        {

            Goods pepsi = new Goods("Pepsi", Goods.SALE);
            Item i2 = new Item(pepsi, 4, 50);
            Customer x = new Customer("test", 10);
            Bill b1 = new Bill(x);
            b1.addGoods(i2);

            string bill = b1.statement();

            Assert.AreEqual("Счет для test\n\tНазвание\tЦена\tКол-воСтоимость\tСкидка\tСумма\tБонус\n\tPepsi\t\t50\t4\t200\t2\t198\t2\nСумма счета составляет 198\nВы заработали 2 бонусных балов", bill);
        }
        [Test()]
        public void Test6()
        {

            Goods pepsi = new Goods("Pepsi", Goods.SALE);
            Item i2 = new Item(pepsi, 2, 50);
            Customer x = new Customer("test", 10);
            Bill b1 = new Bill(x);
            b1.addGoods(i2);

            string bill = b1.statement();

            Assert.AreEqual("Счет для test\n\tНазвание\tЦена\tКол-воСтоимость\tСкидка\tСумма\tБонус\n\tPepsi\t\t50\t2\t100\t0\t100\t1\nСумма счета составляет 100\nВы заработали 1 бонусных балов", bill);
        }
        [Test()]
        public void Test7()
        {

            Goods fanta = new Goods("Fanta", Goods.SPECIAL_OFFER);
            Item i3 = new Item(fanta, 12, 44);
            Customer x = new Customer("test", 10);
            Bill b1 = new Bill(x);
            b1.addGoods(i3);

            string bill = b1.statement();

            Assert.AreEqual("Счет для test\n\tНазвание\tЦена\tКол-воСтоимость\tСкидка\tСумма\tБонус\n\tFanta\t\t44\t12\t528\t2,64\t518\t0\nСумма счета составляет 518\nВы заработали 0 бонусных балов", bill);
        }
        [Test()]
        public void Test8()
        {

            Goods fanta = new Goods("Fanta", Goods.SPECIAL_OFFER);
            Item i3 = new Item(fanta, 9, 44);
            Customer x = new Customer("test", 10);
            Bill b1 = new Bill(x);
            b1.addGoods(i3);

            string bill = b1.statement();
            Assert.AreEqual("Счет для test\n\tНазвание\tЦена\tКол-воСтоимость\tСкидка\tСумма\tБонус\n\tFanta\t\t44\t9\t396\t0\t386\t0\nСумма счета составляет 386\nВы заработали 0 бонусных балов", bill);
        }

        [Test()]
        public void Test9()
        {
            Goods fanta = new Goods("Fanta", Goods.SPECIAL_OFFER);
            Item i3 = new Item(fanta, 0, 44);
            Customer x = new Customer("test", 10);
            Bill b1 = new Bill(x);
            b1.addGoods(i3);

            string bill = b1.statement();
            Assert.AreEqual("Счет для test\n\tНазвание\tЦена\tКол-воСтоимость\tСкидка\tСумма\tБонус\n\tFanta\t\t44\t0\t0\t0\t0\t0\nСумма счета составляет 0\nВы заработали 0 бонусных балов", bill);
        }
        [Test()]
        public void Test10()
        {
            Goods cola = new Goods("Cola", Goods.REGULAR);
            Item i1 = new Item(cola, 4, 65);
            Customer x = new Customer("test", 10);

            Bill b1 = new Bill(x);
            b1.addGoods(i1);


            string bill = b1.statement();

            Assert.AreEqual("Счет для test\n\tНазвание\tЦена\tКол-воСтоимость\tСкидка\tСумма\tБонус\n\tCola\t\t65\t4\t260\t7,8\t252,2\t13\nСумма счета составляет 252,2\nВы заработали 13 бонусных балов", bill);

        }
    }
}
