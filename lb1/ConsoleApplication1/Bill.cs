﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvolutionLb1
{ // отвечает за представление чека, учет его содержимого
     public class Bill
    {
        private List<Item> _items; 
        private Customer _customer;
        String result;
        public Bill(Customer customer) 
        { 
            this._customer = customer; 
            this._items = new List<Item>(); 
        } 
  
        public void addGoods(Item arg) 
        { 
            _items.Add(arg); 
        }

        public void GetHeader() 
        {
            result = "Счет для " + _customer.getName() + "\n"
                     +"\t" + "Название" + "\t" + "Цена" +
                      "\t" + "Кол-во" + "Стоимость" + "\t" + "Скидка" +
                      "\t" + "Сумма" + "\t" + "Бонус" + "\n";   
        }


        public void GetItemString(Item each, double discount, int bonus, double thisAmount) 
        {
           result += "\t" + each.getGoods().getTitle() 
                                       + "\t" 
                                       + "\t" + each.getPrice() 
                                       + "\t" + each.getQuantity() + 
                                         "\t" + (each.getQuantity() * each.getPrice()).ToString() 
                                       + "\t" + discount.ToString() + "\t" + thisAmount.ToString() 
                                       + "\t" + bonus.ToString() + "\n";
        }

         
        public void GetFooter(double totalAmount, int totalBonus) 
        {
            result += "Сумма счета составляет " 
                                            + totalAmount.ToString() 
                                            + "\n"
                                            + "Вы заработали " 
                                            + totalBonus.ToString() + " бонусных балов";
        }


        //public int GetBonus(int bonus, Item each)
        //{
        //    bonus = 0;

        //    switch (each.getGoods().getPriceCode())
        //    {
        //        case Goods.REGULAR:             
        //            bonus = (int)(each.getQuantity() * each.getPrice() * 0.05);
        //            break;           
        //        case Goods.SALE:                     
        //            bonus = (int)(each.getQuantity() * each.getPrice() * 0.01);
        //            break;
        //    }
        //    return bonus;
        //}

        //public double GetDiscount(double discount, Item each)
        //{
        //    discount = 0;

        //    switch (each.getGoods().getPriceCode())
        //    {
        //        case Goods.REGULAR:
        //            if (each.getQuantity() > 2)
        //                discount = (each.getQuantity() * each.getPrice()) * 0.03; // 3%
        //            break;
        //        case Goods.SPECIAL_OFFER:
        //            if (each.getQuantity() > 10)
        //                discount = (each.getQuantity() * each.getPrice()) * 0.005; // 0.5%
        //            break;
        //        case Goods.SALE:
        //            if (each.getQuantity() > 3)
        //                discount = (each.getQuantity() * each.getPrice()) * 0.01; // 0.1%
        //            break;
        //    }

        //    return discount;
        //}

        public double GetUsedBonus(Item each, double discount) 
        {
            double uBonus = 0;

            switch (each.getGoods().getPriceCode())
            {
                case Goods.REGULAR:
                    if (each.getQuantity() > 5)
                        uBonus += _customer.useBonus((int)(each.getQuantity() * each.getPrice())) - discount;
                    break;
                case Goods.SPECIAL_OFFER:
                    if (each.getQuantity() > 1)
                        uBonus += _customer.useBonus((int)(each.getQuantity() * each.getPrice())) - discount;
                    break;
            }
            return uBonus;

        }

        //public double GetSum(Item each) 
        //{
        //    return each.getQuantity() * each.getPrice(); 
        //}
        public String statement() 
        { 
            double totalAmount = 0;
            int totalBonus = 0;
            double usedBonus;
            List<Item>.Enumerator items = _items.GetEnumerator();

            GetHeader();

            while (items.MoveNext()) 
            {
                //удалил thisAmount
                    double thisAmount = 0;
                    Item each = (Item)items.Current;          
                    //определить сумму для каждой строки 

                    usedBonus = GetUsedBonus(each, each.GetDiscount(each));
                    // сумма 
                    // используем бонусы 
                     // учитываем скидку 
                    thisAmount = each.GetSum(each) - each.GetDiscount(each) - usedBonus;
                    GetItemString(each, each.GetDiscount(each), each.GetBonus(each), thisAmount); 
                        totalAmount += thisAmount; 
                        totalBonus += each.GetBonus(each); 
                }
                        GetFooter(totalAmount, totalBonus);
                        //Запомнить бонус клиента 
                        _customer.receiveBonus(totalBonus);

                        return result;
            }
    } 
}







