﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvolutionLb1
{ // Класс, представляющий данные о чеке  
    public class Item
    {
        private Goods _Goods;
        private int _quantity;
        private double _price;

        public Item(Goods Goods, int quantity, double price)
        {
            _Goods = Goods;
            _quantity = quantity;
            _price = price;
        }

        public int getQuantity()
        {
            return _quantity;
        }

        public double getPrice()
        {
            return _price;
        }

        public Goods getGoods()
        {
            return _Goods;
        }


        public int GetBonus(Item each)
        {
            return getGoods().GetBonus(this);
        }

        public double GetDiscount(Item each) 
        {
            return getGoods().GetDiscount(this);
        }

        public double GetSum(Item each) 
        {
            return getGoods().GetSum(each);
        }
    }
} 


